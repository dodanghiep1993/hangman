package gameplay;

import entity.Player;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.HashSet;
import java.util.List;
import java.util.Scanner;

public class Hangman {

    public static void main(String[] args) {
        String toplay = "";
        HashSet<String> names = new HashSet<>();
        //GAME:
        do {
            Scanner getInput = new Scanner(System.in);
            System.out.print("Enter your name: ");
            String name = getInput.next();
            System.out.println("\n" + "HANGMAN\n" + "\n");
            String[] words = {"ninja", "integer", "boolean", "power", "hackathon", "whatever"};
            String secret_word = words[(int) (Math.random() * words.length)];
            System.out.println(secret_word);
            System.out.println("\n");
            char[] sw = secret_word.toCharArray();
            char[] answer = new char[sw.length];
            int lives = 6;
            int score = 1;
            StringBuilder str = new StringBuilder();

            // fill in answer chararray with question mark.
            for (int i = 0; i < answer.length; i ++) {
                answer[i] = '_';
            }

            //run the program while finished is false;
            boolean finished = false;

            while(finished == false) {
                boolean found = false;
                drawHangman(lives);

                System.out.println("\n" + "Missed letters: " + str + "\n");

                //Checking the process to see how far the users doing
                for (int i = 0; i < answer.length; i++) {
                    System.out.print(answer[i]);
                }
                System.out.println("\n");
                System.out.println("Guess a letter.\n" + "\n");
                String input = "";
                while (true) {
                    try {
                        if(getInput.hasNextInt()) {
                            throw new Exception();
                        } else {
                            input = getInput.next();
                            break;
                        }
                    } catch (Exception e) {
                        getInput.next();
                        System.out.println();
                        System.out.println();
                        System.out.println("Invalid input! input should be only one letter\n" + "\n");
                        //break GAME;
                    }
                }

                System.out.println("\n");

                //Check to see if the users repeat inputs
                if ((new String(answer).contains(input)) || (str.toString().contains(input))) {
                    System.out.println("You have already guessed that letter. Choose again.\n" + "\n");
                    found = true;
                }

                for (int i = 0; i < sw.length; i++) {
                    if (input.charAt(0) == sw[i]) {
                        answer[i] = sw[i];
                        found = true;
                    }
                }

                if (!(new String(answer).contains(input)) && !(new String(str).contains(input))) {
                    str.append(input.charAt(0));
                }

                if(!found) {
                    lives--;
                }

                if((new String(answer).contains(new String(sw)))) {
                    System.out.println("Yes! The secret word is \"" + new String(sw) + "\"! You have won!");
                    if (names.contains(name)) {
                        updateWin(name, score);
                    } else {
                        names.add(name);
                        addWin(name);
                    }
                    showPlayer(name);
                    finished = true;
                }

                if (lives <= 0) {
                    drawHangman(lives);
                    System.out.println("You are dead! The secret word is \"" + new String(sw) + "\"");
                    if (names.contains(name)) {
                        updateLose(name, score);
                    } else {
                        names.add(name);
                        addLose(name);
                    }
                    showPlayer(name);
                    finished = true;
                }
            }
            System.out.println("Do you want to play again? (yes or no)\n" + "\n");
            try {
                if (getInput.hasNextInt()) {
                    throw new Exception();
                } else {
                    toplay = getInput.next();
                }
            } catch (Exception e) {
                System.out.println();
                System.out.println();
                System.out.println("Invalid input! if you don't know how to type yes or no " +
                                   " You should not play this game");
                break;
            }
        } while (toplay.equals("yes"));
        deleteAll();
    }

    // print Hangman according to lives
    public static void drawHangman (int l) {
        if(l == 6) {
            System.out.println("|----------");
            System.out.println("|");
            System.out.println("|");
            System.out.println("|");
            System.out.println("|");
            System.out.println("|");
            System.out.println("|");
        }
        else if(l == 5) {
            System.out.println("|----------");
            System.out.println("|    O");
            System.out.println("|");
            System.out.println("|");
            System.out.println("|");
            System.out.println("|");
            System.out.println("|");
        }
        else if(l == 4) {
            System.out.println("|----------");
            System.out.println("|    O");
            System.out.println("|    |");
            System.out.println("|");
            System.out.println("|");
            System.out.println("|");
            System.out.println("|");
        }
        else if(l == 3) {
            System.out.println("|----------");
            System.out.println("|    O");
            System.out.println("|   -|");
            System.out.println("|");
            System.out.println("|");
            System.out.println("|");
            System.out.println("|");
        }
        else if(l == 2) {
            System.out.println("|----------");
            System.out.println("|    O");
            System.out.println("|   -|-");
            System.out.println("|");
            System.out.println("|");
            System.out.println("|");
            System.out.println("|");
        }
        else if(l == 1) {
            System.out.println("|----------");
            System.out.println("|    O");
            System.out.println("|   -|-");
            System.out.println("|   /");
            System.out.println("|");
            System.out.println("|");
            System.out.println("|");
        }
        else{
            System.out.println("|----------");
            System.out.println("|    O");
            System.out.println("|   -|-");
            System.out.println("|   /|");
            System.out.println("|");
            System.out.println("|");
            System.out.println("|");
        }
    }

    public static void showPlayer (String playerName) {

        SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
                .addAnnotatedClass(Player.class)
                .buildSessionFactory();

        try {
            Session session = factory.getCurrentSession();
            session.beginTransaction();

            List<Player> myPlayers = session.createQuery("from Player where playerName='" + playerName + '\'').getResultList();
            for (Player tempPlayer: myPlayers) {
                System.out.println(tempPlayer);
            }
            session.getTransaction().commit();
        } finally {
            factory.close();
        }
    }

    public static void deleteAll () {
        //create session factory this is for hibernate
        SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
                .addAnnotatedClass(Player.class)
                .buildSessionFactory();

        try {
            Session session = factory.getCurrentSession();
            session.beginTransaction();

            List<Player> allPlayers = session.createQuery("from Player").getResultList();

            for (Player tempPlayer: allPlayers) {
                session.delete(tempPlayer);
            }
            session.getTransaction().commit();
        } finally {
            factory.close();
        }
    }

    public static void addWin (String playerName) {

        //create session factory this is for hibernate
        SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
                .addAnnotatedClass(Player.class)
                .buildSessionFactory();
        try {
            Session session = factory.getCurrentSession();
            session.beginTransaction();
            Player myPlayer = new Player(playerName, 1);
            session.save(myPlayer);
            session.getTransaction().commit();
        } finally {
            factory.close();
        }
    }

    public static void updateWin (String playerName, int score) {

        //create session factory this is for hibernate
        SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
                .addAnnotatedClass(Player.class)
                .buildSessionFactory();

        try {
            Session session = factory.getCurrentSession();
            session.beginTransaction();

            //get all player information from player database
            List<Player> thePlayers = session.createQuery("from Player").getResultList();

            Player myPlayer = new Player(playerName, score);

            for (Player tempPlayer: thePlayers) {
                if (tempPlayer.getPlayerName().equals(myPlayer.getPlayerName())) {
                    tempPlayer.setScore(tempPlayer.getScore() + 1);
                }
            }
            session.getTransaction().commit();

        } finally {
            factory.close();
        }
    }

    public static void addLose (String playerName) {

        //create session factory this is for hibernate
        SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
                .addAnnotatedClass(Player.class)
                .buildSessionFactory();
        try {
            Session session = factory.getCurrentSession();
            session.beginTransaction();
            Player myPlayer = new Player(playerName, 0);
            session.save(myPlayer);
            session.getTransaction().commit();
        } finally {
            factory.close();
        }
    }

    public static void updateLose (String playerName, int score) {

        //create session factory this is for hibernate
        SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
                .addAnnotatedClass(Player.class)
                .buildSessionFactory();

        try {
            Session session = factory.getCurrentSession();
            session.beginTransaction();

            //get all player information from player database
            List<Player> thePlayers = session.createQuery("from Player").getResultList();

            Player myPlayer = new Player(playerName, score);

            for (Player tempPlayer: thePlayers) {
                if (tempPlayer.getPlayerName().equals(myPlayer.getPlayerName()) && tempPlayer.getScore() > 0) {
                    tempPlayer.setScore(tempPlayer.getScore() - 1);
                }
            }
            session.getTransaction().commit();

        } finally {
            factory.close();
        }
    }

}
